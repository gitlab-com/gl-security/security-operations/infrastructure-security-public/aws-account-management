output "iam_scp_policy" {
  description = "Policy ID of the SCP to prevent IAM role deletion."
  value       = aws_organizations_policy.prevent_role_modification.id
}
