# ==============================================================================
# CLOUDTRAIL: enable a multi-region cloudtrail forwarding to an s3 bucket
# ==============================================================================

resource "aws_cloudtrail" "cloudtrail_security" {
  #checkov:skip=CKV2_AWS_10: "Ensure CloudTrail trails are integrated with CloudWatch Logs"
  #checkov:skip=CKV_AWS_35: "Ensure CloudTrail logs are encrypted at rest using KMS CMKs"
  #checkov:skip=CKV_AWS_252:  "Ensure CloudTrail defines an SNS Topic"
  name           = "organizational_cloudtrail_security"
  s3_bucket_name = var.cloudtrail_logs_s3_bucket == null ? aws_s3_bucket.audit_logs[0].id : var.cloudtrail_logs_s3_bucket
  kms_key_id     = var.cloudtrail_logs_kms_key_id == null ? aws_kms_key.audit_logs_key[0].arn : var.cloudtrail_logs_kms_key_id

  enable_logging                = true
  is_multi_region_trail         = true
  is_organization_trail         = true
  include_global_service_events = true
  enable_log_file_validation    = true

  tags = var.tags
}

# ==============================================================================
# AUDIT LOGS BUCKET - This bucket will be used to store both CloudTrail Logs
# if `var.cloudtrail_logs_s3_bucket` is not set
# ==============================================================================
#
# Bucket
#
resource "aws_s3_bucket" "audit_logs" {
  #checkov:skip=CKV_AWS_18: "Ensure the S3 bucket has access logging enabled"
  #checkov:skip=CKV_AWS_21: "Ensure all data stored in the S3 bucket have versioning enabled"
  #checkov:skip=CKV_AWS_144: "We don't need cross-region replication."
  count = var.cloudtrail_logs_s3_bucket == null ? 1 : 0

  bucket_prefix = "audit-logs"
  acl           = "log-delivery-write"

  lifecycle_rule {
    id      = "bucket-cleanup"
    enabled = true
    expiration {
      days                         = var.cloudtrail_logs_internal_bucket_lifecycle_rule_expiration_days
      expired_object_delete_marker = false
    }
    noncurrent_version_expiration {
      days = var.cloudtrail_logs_internal_bucket_lifecycle_rule_expiration_days
    }
    abort_incomplete_multipart_upload_days = 30
  }

  # Enable default server side encryption
  server_side_encryption_configuration {
    rule {
      bucket_key_enabled = true
      apply_server_side_encryption_by_default {
        kms_master_key_id = var.cloudtrail_logs_kms_key_id == null ? aws_kms_key.audit_logs_key[0].arn : var.cloudtrail_logs_kms_key_id
        sse_algorithm     = "aws:kms"
      }
    }
  }

  tags = var.tags
}

#
# Public Access Block
#
resource "aws_s3_bucket_public_access_block" "audit_logs_block" {
  count = var.cloudtrail_logs_s3_bucket == null ? 1 : 0

  bucket = aws_s3_bucket.audit_logs[0].id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

#
# Bucket Policy
#
resource "aws_s3_bucket_policy" "audit_logs_policy" {
  count = var.cloudtrail_logs_s3_bucket == null ? 1 : 0

  bucket = aws_s3_bucket.audit_logs[0].id
  policy = data.aws_iam_policy_document.audit_logs_policy[0].json
}

data "aws_iam_policy_document" "audit_logs_policy" {
  count = var.cloudtrail_logs_s3_bucket == null ? 1 : 0

  statement {
    sid = "AWSCloudTrailAclCheck"
    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
    actions   = ["s3:GetBucketAcl"]
    resources = ["${aws_s3_bucket.audit_logs[0].arn}"]
  }

  statement {
    sid = "AWSCloudTrailWrite"
    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
    actions   = ["s3:PutObject"]
    resources = ["${aws_s3_bucket.audit_logs[0].arn}/*"]
    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values = [
        "bucket-owner-full-control",
      ]
    }
  }
}

#
# Encryption via KMS
#
resource "aws_kms_key" "audit_logs_key" {
  count = var.cloudtrail_logs_kms_key_id == null ? 1 : 0

  description             = "This key is used to encrypt Cloudtrail logs and objects of the audit_logs bucket"
  deletion_window_in_days = 10
  enable_key_rotation     = true

  policy = data.aws_iam_policy_document.audit_logs_kms_policy.json

  tags = var.tags
}

data "aws_iam_policy_document" "audit_logs_kms_policy" {
  #checkov:skip=CKV_AWS_109: "Ensure IAM policies does not allow permissions management / resource exposure without constraints"
  #checkov:skip=CKV_AWS_111: "Ensure IAM policies does not allow write access without constraints"
  statement {
    sid    = "Allow CloudTrail to encrypt/decrypt logs"
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey",
    ]
    resources = [
      "*"
    ]
  }

  statement {
    sid    = "Allow root and Terraform to manage the key"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${local.account_id}:root"]
    }
    actions = [
      "kms:*",
    ]
    resources = [
      "*"
    ]
  }
}
