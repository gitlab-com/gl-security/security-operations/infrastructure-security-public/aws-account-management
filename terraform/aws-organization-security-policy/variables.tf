variable "regions" {
  description = "Regions in which to enable GuardDuty."
  // New regions must also be added on the guardduty.tf
  default = [
    "us-east-1",
    "us-east-2",
    "us-west-1",
    "us-west-2",
    "ca-central-1",
    "eu-central-1",
    "eu-west-1",
    "eu-west-2",
    "eu-west-3",
    "ap-northeast-1",
    "ap-northeast-2",
    "ap-southeast-1",
    "ap-southeast-2",
    "ap-south-1",
    "sa-east-1",
  ]
}

variable "cloudtrail_logs_s3_bucket" {
  description = "(Optional) Name of the S3 bucket where to ship Cloudtrail logs. If not set, logs will be sent to Cloudwatch."
  type        = string
  default     = null
}

variable "cloudtrail_logs_kms_key_id" {
  description = "(Optional) KMS key ARN to use to encrypt the logs delivered by CloudTrail."
  type        = string
  default     = null
}

variable "cloudtrail_logs_internal_bucket_lifecycle_rule_expiration_days" {
  description = "(Used when `cloudtrail_logs_s3_bucket` is not set) Specifies the number of days after object creation when object is deleted."
  type        = number
  default     = 90

}

variable "guardduty_publish_s3_bucket_name" {
  description = "Name of the S3 bucket where where to ship GuardDuty findings."
  type        = string
  validation {
    condition     = length(var.guardduty_publish_s3_bucket_name) > 0
    error_message = "The variable guardduty_publish_s3_bucket_name MUST be set."
  }
}

variable "guardduty_publish_kms_key_arn" {
  description = "The ARN of the KMS key used to encrypt GuardDuty findings. Required if publish_bucket_name is set."
  type        = string
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "scp_role_principal_list" {
  description = "List of roles to their respective allowed principal ARNs. Specify only the string that comes after role/ in the ARN."
  type        = list(string)
  default = [
    "OrganizationAccountAccessRole",
  ]
}
