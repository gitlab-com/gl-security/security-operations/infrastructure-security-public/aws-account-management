data "aws_iam_policy_document" "prevent_role_modification" {
  statement {
    effect = "Deny"

    actions = [
      "iam:AttachRolePolicy",
      "iam:DeleteRole",
      "iam:DeleteRolePermissionsBoundary",
      "iam:DeleteRolePolicy",
      "iam:DetachRolePolicy",
      "iam:PutRolePermissionsBoundary",
      "iam:PutRolePolicy",
      "iam:UpdateAssumeRolePolicy",
      "iam:UpdateRole",
      "iam:UpdateRoleDescription"
    ]
    resources = formatlist("arn:aws:iam::*:role/%s", var.scp_role_principal_list)
    condition {
      test     = "StringNotLike"
      variable = "aws:PrincipalArn"
      values   = formatlist("arn:aws:iam::*:role/%s", var.scp_role_principal_list)
    }
  }
}

resource "aws_organizations_policy" "prevent_role_modification" {
  name        = "PreventRoleModification"
  description = "Restrict specific actions on roles based on allowed principals for each role"
  content     = data.aws_iam_policy_document.prevent_role_modification.json
}
