# =============================================================================
# Enables guardduty for all the accounts on the organization on the region
# us-east-1
# =============================================================================
provider "aws" {
  alias  = "us-east-1"
  region = "us-east-1"
}

module "us_east_1_guardduty" {
  source = "../aws-organization-single-region-guardduty"
  count  = contains(var.regions, "us-east-1") ? 1 : 0

  providers = {
    aws = aws.us-east-1
  }
  aws_accounts             = data.aws_organizations_organization.org.non_master_accounts
  admin_account            = local.account_id
  publish_s3_bucket_name   = var.guardduty_publish_s3_bucket_name
  publish_s3_bucket_prefix = "GuardDutyFindings"
  publish_kms_key_arn      = var.guardduty_publish_kms_key_arn
  tags                     = var.tags
}

# =============================================================================
# Enables guardduty for all the accounts on the organization on the region
# us-east-2
# =============================================================================
provider "aws" {
  alias  = "us-east-2"
  region = "us-east-2"
}

module "us_east_2_guardduty" {
  source = "../aws-organization-single-region-guardduty"
  providers = {
    aws = aws.us-east-2
  }
  aws_accounts             = data.aws_organizations_organization.org.non_master_accounts
  admin_account            = local.account_id
  publish_s3_bucket_name   = var.guardduty_publish_s3_bucket_name
  publish_s3_bucket_prefix = "GuardDutyFindings"
  publish_kms_key_arn      = var.guardduty_publish_kms_key_arn
  tags                     = var.tags
}

# =============================================================================
# Enables guardduty for all the accounts on the organization on the region
# us-west-1
# =============================================================================
provider "aws" {
  alias  = "us-west-1"
  region = "us-west-1"
}

module "us_west_1_guardduty" {
  source = "../aws-organization-single-region-guardduty"
  providers = {
    aws = aws.us-west-1
  }
  aws_accounts             = data.aws_organizations_organization.org.non_master_accounts
  admin_account            = local.account_id
  publish_s3_bucket_name   = var.guardduty_publish_s3_bucket_name
  publish_s3_bucket_prefix = "GuardDutyFindings"
  publish_kms_key_arn      = var.guardduty_publish_kms_key_arn
  tags                     = var.tags
}

# =============================================================================
# Enables guardduty for all the accounts on the organization on the region
# us-west-2
# =============================================================================
provider "aws" {
  alias  = "us-west-2"
  region = "us-west-2"
}

module "us_west_2_guardduty" {
  source = "../aws-organization-single-region-guardduty"
  providers = {
    aws = aws.us-west-2
  }
  aws_accounts             = data.aws_organizations_organization.org.non_master_accounts
  admin_account            = local.account_id
  publish_s3_bucket_name   = var.guardduty_publish_s3_bucket_name
  publish_s3_bucket_prefix = "GuardDutyFindings"
  publish_kms_key_arn      = var.guardduty_publish_kms_key_arn
  tags                     = var.tags
}

# =============================================================================
# Enables guardduty for all the accounts on the organization on the region
# eu-central-1
# =============================================================================
provider "aws" {
  alias  = "eu-central-1"
  region = "eu-central-1"
}

module "eu_central_1_guardduty" {
  source = "../aws-organization-single-region-guardduty"
  providers = {
    aws = aws.eu-central-1
  }
  aws_accounts             = data.aws_organizations_organization.org.non_master_accounts
  admin_account            = local.account_id
  publish_s3_bucket_name   = var.guardduty_publish_s3_bucket_name
  publish_s3_bucket_prefix = "GuardDutyFindings"
  publish_kms_key_arn      = var.guardduty_publish_kms_key_arn
  tags                     = var.tags
}

# =============================================================================
# Enables guardduty for all the accounts on the organization on the region
# eu-west-1
# =============================================================================
provider "aws" {
  alias  = "eu-west-1"
  region = "eu-west-1"
}

module "eu_west_1_guardduty" {
  source = "../aws-organization-single-region-guardduty"
  providers = {
    aws = aws.eu-west-1
  }
  aws_accounts             = data.aws_organizations_organization.org.non_master_accounts
  admin_account            = local.account_id
  publish_s3_bucket_name   = var.guardduty_publish_s3_bucket_name
  publish_s3_bucket_prefix = "GuardDutyFindings"
  publish_kms_key_arn      = var.guardduty_publish_kms_key_arn
  tags                     = var.tags
}

# =============================================================================
# Enables guardduty for all the accounts on the organization on the region
# eu-west-2
# =============================================================================
provider "aws" {
  alias  = "eu-west-2"
  region = "eu-west-2"
}

module "eu_west_2_guardduty" {
  source = "../aws-organization-single-region-guardduty"
  providers = {
    aws = aws.eu-west-2
  }
  aws_accounts             = data.aws_organizations_organization.org.non_master_accounts
  admin_account            = local.account_id
  publish_s3_bucket_name   = var.guardduty_publish_s3_bucket_name
  publish_s3_bucket_prefix = "GuardDutyFindings"
  publish_kms_key_arn      = var.guardduty_publish_kms_key_arn
  tags                     = var.tags
}

# =============================================================================
# Enables guardduty for all the accounts on the organization on the region
# eu-west-3
# =============================================================================
provider "aws" {
  alias  = "eu-west-3"
  region = "eu-west-3"
}

module "eu_west_3_guardduty" {
  source = "../aws-organization-single-region-guardduty"
  providers = {
    aws = aws.eu-west-3
  }
  aws_accounts             = data.aws_organizations_organization.org.non_master_accounts
  admin_account            = local.account_id
  publish_s3_bucket_name   = var.guardduty_publish_s3_bucket_name
  publish_s3_bucket_prefix = "GuardDutyFindings"
  publish_kms_key_arn      = var.guardduty_publish_kms_key_arn
  tags                     = var.tags
}

# =============================================================================
# Enables guardduty for all the accounts on the organization on the region
# ap-northeast-1
# =============================================================================
provider "aws" {
  alias  = "ap-northeast-1"
  region = "ap-northeast-1"
}

module "ap_northeast_1_guardduty" {
  source = "../aws-organization-single-region-guardduty"
  providers = {
    aws = aws.ap-northeast-1
  }
  aws_accounts             = data.aws_organizations_organization.org.non_master_accounts
  admin_account            = local.account_id
  publish_s3_bucket_name   = var.guardduty_publish_s3_bucket_name
  publish_s3_bucket_prefix = "GuardDutyFindings"
  publish_kms_key_arn      = var.guardduty_publish_kms_key_arn
  tags                     = var.tags
}

# =============================================================================
# Enables guardduty for all the accounts on the organization on the region
# ap-northeast-2
# =============================================================================
provider "aws" {
  alias  = "ap-northeast-2"
  region = "ap-northeast-2"
}

module "ap_northeast_2_guardduty" {
  source = "../aws-organization-single-region-guardduty"
  providers = {
    aws = aws.ap-northeast-2
  }
  aws_accounts             = data.aws_organizations_organization.org.non_master_accounts
  admin_account            = local.account_id
  publish_s3_bucket_name   = var.guardduty_publish_s3_bucket_name
  publish_s3_bucket_prefix = "GuardDutyFindings"
  publish_kms_key_arn      = var.guardduty_publish_kms_key_arn
  tags                     = var.tags
}

# =============================================================================
# Enables guardduty for all the accounts on the organization on the region
# ap-southeast-1
# =============================================================================
provider "aws" {
  alias  = "ap-southeast-1"
  region = "ap-southeast-1"
}

module "ap_southeast_1_guardduty" {
  source = "../aws-organization-single-region-guardduty"
  providers = {
    aws = aws.ap-southeast-1
  }
  aws_accounts             = data.aws_organizations_organization.org.non_master_accounts
  admin_account            = local.account_id
  publish_s3_bucket_name   = var.guardduty_publish_s3_bucket_name
  publish_s3_bucket_prefix = "GuardDutyFindings"
  publish_kms_key_arn      = var.guardduty_publish_kms_key_arn
  tags                     = var.tags
}

# =============================================================================
# Enables guardduty for all the accounts on the organization on the region
# ap-southeast-2
# =============================================================================
provider "aws" {
  alias  = "ap-southeast-2"
  region = "ap-southeast-2"
}

module "ap_southeast_2_guardduty" {
  source = "../aws-organization-single-region-guardduty"
  providers = {
    aws = aws.ap-southeast-2
  }
  aws_accounts             = data.aws_organizations_organization.org.non_master_accounts
  admin_account            = local.account_id
  publish_s3_bucket_name   = var.guardduty_publish_s3_bucket_name
  publish_s3_bucket_prefix = "GuardDutyFindings"
  publish_kms_key_arn      = var.guardduty_publish_kms_key_arn
  tags                     = var.tags
}

# =============================================================================
# Enables guardduty for all the accounts on the organization on the region
# ap-south-1
# =============================================================================
provider "aws" {
  alias  = "ap-south-1"
  region = "ap-south-1"
}

module "ap_south_1_guardduty" {
  source = "../aws-organization-single-region-guardduty"
  providers = {
    aws = aws.ap-south-1
  }
  aws_accounts             = data.aws_organizations_organization.org.non_master_accounts
  admin_account            = local.account_id
  publish_s3_bucket_name   = var.guardduty_publish_s3_bucket_name
  publish_s3_bucket_prefix = "GuardDutyFindings"
  publish_kms_key_arn      = var.guardduty_publish_kms_key_arn
  tags                     = var.tags
}

# =============================================================================
# Enables guardduty for all the accounts on the organization on the region
# sa-east-1
# =============================================================================
provider "aws" {
  alias  = "sa-east-1"
  region = "sa-east-1"
}

module "sa_east_1_guardduty" {
  source = "../aws-organization-single-region-guardduty"
  providers = {
    aws = aws.sa-east-1
  }
  aws_accounts             = data.aws_organizations_organization.org.non_master_accounts
  admin_account            = local.account_id
  publish_s3_bucket_name   = var.guardduty_publish_s3_bucket_name
  publish_s3_bucket_prefix = "GuardDutyFindings"
  publish_kms_key_arn      = var.guardduty_publish_kms_key_arn
  tags                     = var.tags
}
