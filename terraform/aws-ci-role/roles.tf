# ==============================================================================
# IAM POLICIES
# ==============================================================================
data "aws_iam_policy_document" "gitlab_allow" {
  count = length(var.roles)

  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRoleWithWebIdentity"]
    principals {
      type        = "Federated"
      identifiers = [aws_iam_openid_connect_provider.gitlab_oidc.arn]
    }
    condition {
      test     = "StringLike"
      variable = "${aws_iam_openid_connect_provider.gitlab_oidc.url}:sub"
      values   = var.roles[count.index].jwt_subs
    }
  }
}

resource "aws_iam_policy" "role_policy" {
  count = length(var.roles)

  name        = "${var.prefix}-${var.roles[count.index].name_suffix}"
  description = "Permissions for ci role to manage aws resources"

  policy = jsonencode(var.roles[count.index].iam_policy)
}

# ==============================================================================
# ROLES
# ==============================================================================
resource "aws_iam_role" "gitlab_role" {
  count = length(var.roles)

  name               = "${var.prefix}-${var.roles[count.index].name_suffix}"
  assume_role_policy = data.aws_iam_policy_document.gitlab_allow[count.index].json
}

resource "aws_iam_role_policy_attachment" "role_policy_attach" {
  count = length(var.roles)

  role       = aws_iam_role.gitlab_role[count.index].name
  policy_arn = aws_iam_policy.role_policy[count.index].arn
}
