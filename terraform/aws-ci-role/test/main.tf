# ==============================================================================
# REGIONS & UTILS
# ==============================================================================
data "aws_region" "current" {}
data "aws_caller_identity" "current" {}

locals {
  gitlab_url   = "gitlab.com"
  roles_prefix = "gitlab-ci"
  account_id   = data.aws_caller_identity.current.account_id
}

module "aws-organization-security-policy" {
  source     = "../"
  gitlab_url = "https://${local.gitlab_url}"
  prefix     = "gitlab-example"
  roles = [
    {
      ##################
      # Repository: https://gitlab.com/my-org/aws-ci-role
      ##################
      name_suffix = "base-role",
      jwt_subs = [
        "project_path:my-org/aws-ci-role:ref_type:branch:ref:main"
      ],
      iam_policy = {
        Version = "2012-10-17"
        Statement = [
          # Manage all the gitlab-ci-* roles
          {
            Action = [
              "iam:*",
            ]
            Effect   = "Allow"
            Resource = "arn:aws:iam::${local.account_id}:role/${local.roles_prefix}-*"
          },
          # Manage all the gitlab-ci-* policies
          {
            Action = [
              "iam:*",
            ]
            Effect   = "Allow"
            Resource = "arn:aws:iam::${local.account_id}:policy/${local.roles_prefix}-*"
          },
          # Manage the oidc provider
          {
            Action = [
              "iam:*",
            ]
            Effect   = "Allow"
            Resource = "arn:aws:iam::${local.account_id}:oidc-provider/${local.gitlab_url}"
          }
        ]
      }
    }
  ]
}

output "roles" {
  value = module.aws-organization-security-policy.roles
}
