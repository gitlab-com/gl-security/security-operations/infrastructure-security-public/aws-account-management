variable "gitlab_url" {
  description = "The URL for the gitlab installation"
  type        = string
  default     = "https://gitlab.com"
}

variable "prefix" {
  description = "All roles and policy names will have the same prefix eg. `$${prefix}-`"
  type        = string
  default     = "gitlab-ci"
}

variable "roles" {
  description = <<EOT
  "
## Example:
## {
##    # A suffix for the role name. Will be appended with the `prefix` variable.
##    "name_suffix" : "",
##    # List of Gitlab JWT sub fields. Used as filter by AWS sts. Source: https://docs.gitlab.com/ee/ci/cloud_services/#how-it-works
##    "jwt_subs" : [
##        ""
##    ],
##    # IAM Policy attached to the role
##    "iam_policy" : { }
## }
EOT
  type = list(object({
    name_suffix = string
    jwt_subs    = list(string)
    iam_policy  = any
  }))
}
