# AWS CI Role

This Terraform module creates all the resources required to [authenticate from GitLab CI using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/aws/) (OIDC).

<!-- markdownlint-disable MD033 -->
<div align="center">
<img width="500px" src="./images/aws-gitlab-oidc.png"/>
</div>
<!-- markdownlint-enable MD033 -->

The module will create the following resources on AWS:

1. A new [IAM Identity Provider](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_providers_create_oidc.html).
1. One or more IAM Roles that can be used when authenticating through the new IAM Identity Provider.

---

⚠️ WARNING ⚠️

For each AWS Account, there can only be ONE Identity Provider for ONE GitLab instance (`gitlab.com`, `gitlab.example.com`, etc...). This module is meant to be used as a central point to configure the access from ALL repositories into 1 AWS Account.

From [AWS Docs](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_providers_create_oidc.html): "Within your AWS account, each IAM OIDC identity provider must use a unique URL"

⚠️ END WARNING ⚠️

---

## Usage

### Roles structure

Within the GitLab CI pipeline, we will use the generated `CI_JOB_JWT_V2` token to authenticate to AWS.

For each IAM Role, we need to define the following:

1. **Role Name** - The name of the role. All the roles will be appended a prefix.
1. **Conditions** - A list of AWS ["StringLike" condition operator](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_elements_condition_operators.html#Conditions_String), to limit which `CI_JOB_JWT_V2` tokens can assume the IAM Role.
1. **IAM Policy** - The IAM policy that defines the permissions of the IAM Role.

In terraform, definition for each role has the following structure:

```hcl
{
    # The role name
    "name_suffix" : "",
    # Conditions - Source: https://docs.gitlab.com/ee/ci/cloud_services/#how-it-works
    "jwt_subs" : [
        ""
    ],
    # IAM Policy attached to the role
    "iam_policy" : { }
}
```

### Configure the GitLab CI pipeline

The pipeline can be easily configured by adding the following `before_script` to the steps on the `.gitlab-ci.yaml` that need AWS credentials:

```yaml
# Assume role on AWS using GitLab's $CI_JOB_JWT_V2
# https://docs.gitlab.com/ee/ci/cloud_services/aws/
.aws_auth:
  before_script:
    - apk add python3
    - python3 -m ensurepip --upgrade && pip3 install awscli
    - >
      STS=$(aws sts assume-role-with-web-identity
      --role-arn ${AWS_ROLE_ARN}
      --role-session-name "GitLabRunner-${CI_PROJECT_ID}-${CI_PIPELINE_ID}"
      --web-identity-token $CI_JOB_JWT_V2
      --duration-seconds 3600
      --query 'Credentials.[AccessKeyId,SecretAccessKey,SessionToken]'
      --output text)
    - export AWS_ACCESS_KEY_ID=`echo $STS | awk '{print $1}'`
    - export AWS_SECRET_ACCESS_KEY=`echo $STS | awk '{print $2}'`
    - export AWS_SESSION_TOKEN=`echo $STS | awk '{print $3}'`

# test credentials
aws:
  variables:
    AWS_ROLE_ARN: 'arn:aws:iam::{REPLACE_ACCOUNT_ID}:role/{REPLACE_ROLE_NAME}'
  script:
    - aws sts get-caller-identity
  extends:
    - .aws_auth
```

### Examples

#### 1. Route53 Audit Project

Let's assume that we have the following scenario where we run some tool to audit all the Route53 resource.

1. **Role Name** - `aws-route53-auditor`
1. **Conditions** - This role can be assumed, when the `CI_JOB_JWT_V2` tokens used to authenticate, matches the following content:
   - The pipeline runs on the following GitLab Repository: `gitlab-com/gl-security/security-operations/infrastructure-security-public/aws-route53-auditor`
   - The pipeline must have been triggered from the `main` `branch`.
1. **IAM Policy** - Full read on AWS Route53.

This results in the following configuration:

```hcl
locals {
  gitlab_url   = "gitlab.com"
  roles_prefix = "gitlab-ci"
}

module "aws-organization-security-policy" {
  source = "git::https://gitlab.com/gitlab-com/gl-security/security-operations/infrastructure-security-public/aws-account-management//terraform/aws-organization-security-policy?ref=main"
  gitlab_url = "https://${local.gitlab_url}"
  prefix     = "${locals.roles_prefix}"
  roles = [
    {
      name_suffix = "base-role",
      jwt_subs = [
        "project_path:project_path:gitlab-com/gl-security/security-operations/infrastructure-security-public/aws-security-auditor:ref_type:branch:ref:main"
      ],
      iam_policy = {
        Version = "2012-10-17"
        Statement = [
          # AmazonRoute53ReadOnlyAccess
          {
            Action = [
              "route53:Get*",
              "route53:List*",
              "route53:TestDNSAnswer"
            ]
            Effect   = "Allow"
            Resource = "*"
          }
        ]
      }
    }
  ]
}

output "roles" {
  value = module.aws-organization-security-policy.roles
}
```

#### 2. Use OIDC to manage the OIDC Provider - Manage itself

It is possible to use OIDC to deploy the resources of this Terraform module.
During the initial deployment, we just need to create an IAM role, that allows
subsequent runs to be done through the pipeline.

1. **Role Name** - `aws-ci-role`
1. **Conditions** - This role can be assumed, when the `CI_JOB_JWT_V2` tokens used to authenticate, matches the following content:
   - The pipeline runs on the following GitLab Repository: `gitlab-com/gl-security/security-operations/infrastructure-security-public/aws-ci-role`
   - The pipeline must have been triggered from the `main` `branch`.
1. **IAM Policy** - Full Read and Write access to all IAM Roles and policies that have the prefix `gitlab-ci`.

```hcl
data "aws_region" "current" {}
data "aws_caller_identity" "current" {}

locals {
  gitlab_url   = "gitlab.com"
  roles_prefix = "gitlab-ci"
  account_id   = data.aws_caller_identity.current.account_id
}

module "aws-organization-security-policy" {
  source     = "../"
  gitlab_url = "https://${local.gitlab_url}"
  prefix     = "${locals.roles_prefix}"
  roles = [
    {
      name_suffix = "base-role",
      jwt_subs = [
        "project_path:gitlab-com/gl-security/security-operations/infrastructure-security-public/aws-ci-role:ref_type:branch:ref:main"
      ],
      iam_policy = {
        Version = "2012-10-17"
        Statement = [
          # Manage all the gitlab-ci-* roles
          {
            Action = [
              "iam:*",
            ]
            Effect   = "Allow"
            Resource = "arn:aws:iam::${local.account_id}:role/${local.roles_prefix}-*"
          },
          # Manage all the gitlab-ci-* policies
          {
            Action = [
              "iam:*",
            ]
            Effect   = "Allow"
            Resource = "arn:aws:iam::${local.account_id}:policy/${local.roles_prefix}-*"
          },
          # Manage the oidc provider
          {
            Action = [
              "iam:*",
            ]
            Effect   = "Allow"
            Resource = "arn:aws:iam::${local.account_id}:oidc-provider/${local.gitlab_url}"
          }
        ]
      }
    }
  ]
}

output "roles" {
  value = module.aws-organization-security-policy.roles
}
```
