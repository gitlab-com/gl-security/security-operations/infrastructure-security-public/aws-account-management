# ==============================================================================
# GITLAB OIDC AWS ROLE
# ==============================================================================
data "tls_certificate" "gitlab" {
  url = var.gitlab_url
}

resource "aws_iam_openid_connect_provider" "gitlab_oidc" {
  // https://docs.gitlab.com/ee/ci/cloud_services/aws/#add-the-identity-provider
  url = var.gitlab_url
  client_id_list = [
    var.gitlab_url
  ]

  thumbprint_list = ["${data.tls_certificate.gitlab.certificates.0.sha1_fingerprint}"]
}
