terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.24"
    }
  }
}

# ============================================================================
# Detector - Creates an instance of GuardDuty that will store and publish
#            findings for this region.
# ============================================================================
resource "aws_guardduty_detector" "detector" {
  enable = true

  tags = var.tags
}

# ============================================================================
# Oranization Admin - Sets the master account as the org admin for GuardDuty
# ============================================================================
resource "aws_guardduty_organization_admin_account" "default" {
  admin_account_id = var.admin_account

  # GuardDuty must be be turned on in the account before we set it as the delegated administrator
  depends_on = [aws_guardduty_detector.detector]
}


# ============================================================================
# Org Configuration - Auto-enables GuardDuty for NEW accounts in the AWS Organization
# ============================================================================
resource "aws_guardduty_organization_configuration" "configuration" {
  auto_enable = true
  detector_id = aws_guardduty_detector.detector.id

  datasources {
    s3_logs {
      auto_enable = true
    }
    kubernetes {
      audit_logs {
        enable = true
      }
    }
    malware_protection {
      scan_ec2_instance_with_findings {
        ebs_volumes {
          auto_enable = true
        }
      }
    }
  }

  depends_on = [
    aws_guardduty_organization_admin_account.default
  ]
}

# ============================================================================
# Guardduty members Configuration - Makes sure all the accounts in the org are
# members. Even if they were created before the
# `aws_guardduty_organization_configuration` was set.
# ============================================================================
resource "aws_guardduty_member" "delegated_administrator" {
  for_each = { for acc in var.aws_accounts : acc.name => acc }

  account_id                 = each.value["id"]
  detector_id                = aws_guardduty_detector.detector.id
  email                      = each.value["email"]
  invite                     = false
  disable_email_notification = false

  lifecycle {
    ignore_changes = [
      email,
      invite,
      disable_email_notification
    ]
  }

  depends_on = [
    aws_guardduty_organization_admin_account.default
  ]
}

# ============================================================================
# GuardDuty publishing destination - Exports findings to an S3 bucket
# ============================================================================
resource "aws_guardduty_publishing_destination" "publish" {
  count = var.publish_s3_bucket_name != "" ? 1 : 0

  detector_id     = aws_guardduty_detector.detector.id
  destination_arn = "arn:aws:s3:::${var.publish_s3_bucket_name}/${var.publish_s3_bucket_prefix}"
  kms_key_arn     = var.publish_kms_key_arn
}
