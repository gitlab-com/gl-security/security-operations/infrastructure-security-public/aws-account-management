variable "aws_accounts" {
  description = "List of aws accounts belonging to the org for which GuardDuty must be enabled"
  type = list(object({
    id    = string
    name  = string
    email = string
  }))
}

variable "admin_account" {
  description = "The id of AWS account that administrates GuardDuty. Must be part of the org."
  type        = string
  validation {
    condition     = length(var.admin_account) == 12
    error_message = "The admin_account is required and must be a valid account id."
  }
}

variable "publish_s3_bucket_name" {
  description = "The name of the s3 bucket to where all the findings will be published (optional)"
  type        = string
  default     = ""
}

variable "publish_s3_bucket_prefix" {
  description = "The prefix will prepend the default folder structure created by GuardDuty, which is /AWSLogs/111122223333/GuardDuty/Region.(optional)"
  type        = string
  default     = ""
}

variable "publish_kms_key_arn" {
  description = "The ARN of the KMS key used to encrypt GuardDuty findings. (required if publish_bucket_name is set)"
  type        = string
  default     = ""
}

variable "tags" {
  type    = map(string)
  default = {}
}
