variable "wiz_data_scanning" {
  type        = string
  default     = "Enabled"
  description = "Toggle this varilable to enable or disable Wiz Data Scanning"
}

variable "wiz_lightsail_scanning" {
  type        = string
  default     = "Disabled"
  description = "Toggle this varilable to enable or disable Wiz LightSail Scanning"
}

variable "wiz_eks_scanning" {
  type        = string
  default     = "Disabled"
  description = "Toggle this varilable to enable or disable Wiz EKS Scanning"
}

variable "wiz_rds_scanning" {
  type        = string
  default     = "Disabled"
  description = "Toggle this varilable to enable or disable Wiz RDS Scanning"
}

variable "wiz_s3_logs_scanning" {
  type        = string
  default     = "Disabled"
  description = "Toggle this varilable to enable or disable Wiz s3 logs Scanning"
}

variable "wiz_s3_kms_decrypt" {
  type        = string
  default     = "Disabled"
  description = "Toggle this varilable to enable or disable Wiz s3 KMS decrypt permissions"
}

variable "wiz_external_id" {
  type        = string
  description = "Pass the Wiz External ID to be configured for authentication"
}

variable "wiz_org_enabled" {
  type        = string
  default     = "Enabled"
  description = "Toggle this varilable to enable or disable the deployed on AWS Org or the Single Account"
}

variable "wiz_sts_role_arn" {
  type        = string
  description = "Pass the Wiz Account ARN that would be used to Assume the role in Member AWS accounts"
}

variable "wiz_role_name" {
  type        = string
  default     = "WizAccess-Role"
  description = "This would define the name of the role that would be created for Wiz to access AWS accounts"
}

variable "wiz_org_unit" {
  type        = string
  default     = ""
  description = "Default would be blank if Org deployment is disable and root id if org deployment is disabled"
}

variable "wiz_stack_name" {
  type        = string
  default     = "SvccAccount"
  description = ""
}

variable "wiz_cf_capabilities" {
  type        = list(string)
  default     = ["CAPABILITY_NAMED_IAM"]
  description = ""
}