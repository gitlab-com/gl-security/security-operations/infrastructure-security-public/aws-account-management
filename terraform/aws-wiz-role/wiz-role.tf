resource "aws_cloudformation_stack" "wiz_access_role" {
  name         = "wiz-${var.wiz_stack_name}"
  capabilities = var.wiz_cf_capabilities
  parameters = {
    DataScanning      = var.wiz_data_scanning
    ExternalId        = var.wiz_external_id
    isOrg             = var.wiz_org_enabled
    LightsailScanning = var.wiz_lightsail_scanning
    EKSScanning       = var.wiz_eks_scanning
    RDSLogsScanning   = var.wiz_rds_scanning
    S3AWSLogsScanning = var.wiz_s3_logs_scanning
    S3KMSDecrypt      = var.wiz_s3_kms_decrypt
    orgId             = var.wiz_org_unit == "" && var.wiz_org_enabled == "Enabled" ? local.root_id : var.wiz_org_unit
    RoleARN           = var.wiz_sts_role_arn
    WizRoleName       = var.wiz_role_name
  }
  template_body = file("${path.module}/wiz_template/wiz-aws-standard-org.json")
}

