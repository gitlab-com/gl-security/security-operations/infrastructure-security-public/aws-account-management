# Wiz IAM Role 

The current deployment of the Wiz IAM role in AWS accounts is done using the CloudFormation template and deploying it using the UI in AWS Management Account.

With the introduction of this module, we are removing the step for using UI and deploying the CloudFormation template using Terraform.

# How the Deployment Works

Below Graph shows how the wiz role is getting deployed.

```mermaid

flowchart TD
    A[User] -->|Wiz CF template Update| B[Repository]
    B[Repository] --> |CI Pipeline| C[Terraform]
    C -->|Deploys|D[CloudFormation Template]
    D -->|Management Account|E[Wiz Role]
    D -->|Org|F[Wiz Role]

```

**High-level Flow**

1. CloudFormation template will be shared by Wiz.
1. The template will be added to the Git Repository.
1. The update would trigger the CI pipelines to deploy the template using Terraform.
1. Terraform runs and deploys the CloudFormation Templates on the AWS Management and member accounts.

# Terraform Module

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.24 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 4.24 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_cloudformation_stack.wiz_access_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudformation_stack) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_organizations_organization.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/organizations_organization) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_wiz_cf_capabilities"></a> [wiz\_cf\_capabilities](#input\_wiz\_cf\_capabilities) | n/a | `list(string)` | <pre>[<br>  "CAPABILITY_NAMED_IAM"<br>]</pre> | no |
| <a name="input_wiz_data_scanning"></a> [wiz\_data\_scanning](#input\_wiz\_data\_scanning) | Toggle this varilable to enable or disable Wiz Data Scanning | `string` | `"Enabled"` | no |
| <a name="input_wiz_external_id"></a> [wiz\_external\_id](#input\_wiz\_external\_id) | Pass the Wiz External ID to be configured for authentication | `string` | n/a | yes |
| <a name="input_wiz_lightsail_scanning"></a> [wiz\_lightsail\_scanning](#input\_wiz\_lightsail\_scanning) | Toggle this varilable to enable or disable Wiz LightSail Scanning | `string` | `"Disabled"` | no |
| <a name="input_wiz_org_enabled"></a> [wiz\_org\_enabled](#input\_wiz\_org\_enabled) | Toggle this varilable to enable or disable the deployed on AWS Org or the Single Account | `string` | `"Disabled"` | no |
| <a name="input_wiz_org_unit"></a> [wiz\_org\_unit](#input\_wiz\_org\_unit) | Default would be blank if Org deployment is disable and root id if org deployment is disabled | `string` | `""` | no |
| <a name="input_wiz_role_name"></a> [wiz\_role\_name](#input\_wiz\_role\_name) | This would define the name of the role that would be created for Wiz to access AWS accounts | `string` | `"WizAccess-Role"` | no |
| <a name="input_wiz_stack_name"></a> [wiz\_stack\_name](#input\_wiz\_stack\_name) | n/a | `string` | `"WizSvccAccount"` | no |
| <a name="input_wiz_sts_role_arn"></a> [wiz\_sts\_role\_arn](#input\_wiz\_sts\_role\_arn) | Pass the Wiz Account ARN that would be used to Assume the role in Member AWS accounts | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->
