# ==============================================================================
# BACKEND AND PROVIDER
# ==============================================================================
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.24"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}

# ==============================================================================
# REGIONS & UTILS
# ==============================================================================
data "aws_region" "current" {}
data "aws_caller_identity" "current" {}
data "aws_organizations_organization" "current" {}

locals {
  root_id = var.wiz_org_enabled == "Enabled" ? data.aws_organizations_organization.current.roots[0].id : ""
}
