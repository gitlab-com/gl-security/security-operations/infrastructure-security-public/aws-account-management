# AWS Security Reviewer

This Cloudformation Stack automates the setup of roles and users needed to perform a security audit of AWS accounts.

In short, this Stack can be used to create:

1. One role (`infrasec-security-audit`) in every AWS account (Hub + all the Spoke ones), with the built-in `SecurityAudit` policy attached to it.
2. One IAM user (`infrasec-cartography`), in the Hub account, able to assume the `infrasec-security-audit` role on all the Spoke accounts. Access keys for this user are not managed via this module.

![](../.gitlab/images/Cross-Account-Auditing-AWS.drawio.png)

## Components

| Component                     | Path             | Description                                                                                                                                                                                       |
| ----------------------------- | ---------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `InfrasecSecurityAudit` Stack | `account-hub/`   | <ul><li>Deploys a user in the Hub, able to assume roles in each Spoke account</li><li>Deployed as a Cloudformation Stack in the Hub account (usually the Management account of the Org)</li></ul> |
| `InfrasecAuditRole` StackSet  | `account-spoke/` | <ul><li>Deploys a SecurityReviewer Role in each account of the Org</li><li>Deployed as a Cloudformation StackSet from the Hub account (usually the Management account of the Org)</li></ul>       |
