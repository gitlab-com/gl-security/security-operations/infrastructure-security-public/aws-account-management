# aws-account-management

[![Generic badge](https://img.shields.io/badge/Team-InfraSec-blue.svg)](https://gitlab.com/groups/gitlab-com/gl-security/security-operations/infrastructure-security/-/wikis/home)
[![Generic badge](https://img.shields.io/badge/Visibility-PUBLIC-red.svg)]()

Automation for managing AWS Organizations/Accounts.

## Components

| Component                 | Usage                                                          |
| ------------------------- | -------------------------------------------------------------- |
| aws-security-reviewer     | [README](aws-security-reviewer/README.md)                      |
| aws-organization-security | [README](terraform/aws-organization-security-policy/README.md) |
